import numpy as np
import tensorflow as tf

import model_config as mcfg
from transformations import generate_rgb_nir_concat_and_label_for_set, \
    get_observation_images, save_observation_plot
from unet import get_unet
from visualization import save_egb_label_pred


if __name__ == '__main__':
    unet_model = get_unet(
        classes_count=mcfg.N_CLASSES, conv2d_activation='leaky_relu', use_inceptions=False)
    optimizer = tf.keras.optimizers.Adam(learning_rate=1e-3, amsgrad=True)
    # optimizer = tf.keras.optimizers.SGD(learning_rate=1e-2, momentum=0.9)

    unet_model.compile(
        optimizer=optimizer, loss='sparse_categorical_crossentropy', metrics=['accuracy'])

    latest_checkpoint = tf.train.latest_checkpoint('checkpoints')
    unet_model.load_weights(latest_checkpoint)

    converter = tf.lite.TFLiteConverter.from_keras_model(unet_model)
    tflite_model = converter.convert()

    with open('model.tflite', 'wb') as f:
        f.write(tflite_model)

