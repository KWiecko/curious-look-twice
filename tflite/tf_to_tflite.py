import os

import tensorflow as tf

if __name__ == '__main__':
    converted_model_dir = '/home/kw/Projects/drone_cv/curious-look-twice/models/best_model'
    converter = tf.lite.TFLiteConverter.from_saved_model(converted_model_dir)
    tflite_model = converter.convert()

    converted_model_name = '07_06_2022_best.tflite'
    saved_model_dir = '/home/kw/Projects/pi_tf_dev/tf_lite_models/'

    save_path = os.sep.join([saved_model_dir, converted_model_name])

    with open(save_path, 'wb') as tflite_model_file:
        tflite_model_file.write(tflite_model)
