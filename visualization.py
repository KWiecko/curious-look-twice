import cv2
import os

import numpy as np


def append_missing_channels(image):
    image_shape = image.shape
    ndims = len(image_shape)
    if ndims == 3:
        return image

    assert 2 <= ndims <= 3

    if ndims == 2:
        filler = np.zeros(image_shape)
        return np.stack([image, filler, filler], axis=-1)
    elif ndims == 3 and image_shape[-1] < 3:
        out = np.zeros((image_shape[0], image_shape[1], 3))
        out[:, :, :image_shape[-1]] = image
        return out
    else:
        raise ValueError('Bad input -> check image or params')


def plot_observation(observation_tuple):
    # make sure only 3D arrays are in observation_tuple
    assert all([2 <= len(oi.shape) <= 3 for oi in observation_tuple])
    assert all([oi.shape[-1] <= 3 for oi in observation_tuple if len(oi.shape) == 3])

    same_dims_images = []
    for oi in observation_tuple:
        same_dims_images.append(append_missing_channels(oi))

    images_to_plot = np.hstack([img for img in same_dims_images]).astype('uint8')
    cv2.imshow('single observation', images_to_plot)
    cv2.waitKey(0)


def save_observation_plot(image_name: str, observation_tuple: tuple, save_dir: str = 'data/overlapping_labels'):
    # make sure only 3D arrays are in observation_tuple
    assert all([2 <= len(oi.shape) <= 3 for oi in observation_tuple])
    assert all([oi.shape[-1] <= 3 for oi in observation_tuple if len(oi.shape) == 3])

    same_dims_images = []
    for oi in observation_tuple:
        same_dims_images.append(append_missing_channels(oi))

    images_to_save = np.hstack([img for img in same_dims_images]).astype('uint8')
    # ensure save dir
    if not os.path.isdir(save_dir):
        os.system(f'mkdir -p {save_dir}')
    observation_png_path = os.sep.join([save_dir, image_name])
    cv2.imwrite(observation_png_path, images_to_save)


def save_egb_label_pred(
        image_name: str, preds_tuple: tuple, save_dir: str = 'data/preds'):
    assert all([2 <= len(oi.shape) <= 3 for oi in preds_tuple])
    assert all([oi.shape[-1] <= 3 for oi in preds_tuple if len(oi.shape) == 3])

    same_dims_images = []
    for oi in preds_tuple:
        same_dims_images.append(append_missing_channels(oi))

    images_to_save = np.hstack([img for img in same_dims_images]).astype('uint8')
    if not os.path.isdir(save_dir):
        os.system(f'mkdir -p {save_dir}')

    observation_png_path = os.sep.join([save_dir, image_name])
    cv2.imwrite(observation_png_path, images_to_save)
