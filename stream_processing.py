import time
from warnings import warn

import cv2
import numpy as np

from threading import Thread


class ThreadedCamera(object):
    def __init__(self, src=0, update_interval_seconds=2):
        self.capture = cv2.VideoCapture(src)
        self.capture.set(cv2.CAP_PROP_BUFFERSIZE, 2)

        # FPS = 1/X
        # X = desired FPS
        if update_interval_seconds < 0.5:
            warn(f'Provided update interval is smaller than average update '
                 f'time - 0.5 seconds. Setting to 0.5seconds')
            update_interval_seconds = 0.5

        self.update_interval_seconds = update_interval_seconds  # update interval in seceonds
        self.set_round_up_to()

        self.wait_key_ms = int(self.update_interval_seconds / 10 * 1000)
        self.wait_key_ms = self.wait_key_ms if self.wait_key_ms > 1 else 1

        # Start frame retrieval thread
        self.thread = Thread(target=self.update, args=())
        self.thread.daemon = True
        self.thread.start()

    def set_round_up_to(self):
        try:
            self.round_up_to = len(
                str(self.update_interval_seconds).split('.')[1])
        except IndexError:
            self.round_up_to = 0

    def update(self):
        already_read = False

        while True:

            time_to_read = round(time.time(), self.round_up_to) % self.update_interval_seconds == 0
            if self.capture.isOpened():
                if time_to_read and not already_read:
                    (self.status, self.frame) = self.capture.read()
                    print(f'Frame shape: {self.frame.shape}')
                    already_read = True
                elif not time_to_read and already_read:
                    self.capture.grab()
                    already_read = False
                else:
                    self.capture.grab()

    def show_frame(self):
        try:
            cv2.imshow('frame', self.frame)
            cv2.waitKey(self.wait_key_ms)
        except Exception as exc:
            # print(f'Following error occurred: {exc}')
            # print(exc)
            pass


class StreamProcessor:
    def __init__(
            self, model=None, stream_frame_size=(720, 1280),
            model_input_size=(512, 512), n_channels: int = 3):
        # alternate res is 1080 x 1920
        # assume input has 3 channels
        # Frame shape: (720, 1280, 3)
        self.stream_frame_size = stream_frame_size
        self.model_input_size = model_input_size
        # for now assume that input always has 3 channels
        assert n_channels == 3
        self.model = model

        self.horizontal_stream_frame_split_indices = None
        self.vertical_stream_frame_split_indices = None

        self.set_frame_split_params()

    def _get_frame_split_indices(self, stream_frame_dimension, model_frame_dimension):
        # every Nth pixel will be splitting stream frame
        split_indices = \
            np.arange(model_frame_dimension, stream_frame_dimension, model_frame_dimension)

        # assume that stream frame last index == M x <model frame size>
        last_split_index = split_indices[-1]
        repeated_indices = split_indices[:-1]
        # if the former is false modify last split index and repeated indices
        # accordingly
        if split_indices[-1] != stream_frame_dimension:
            last_split_index = stream_frame_dimension
            repeated_indices = split_indices

        # multiply indices by 2 to get start and stop indices
        multiplied_indices = np.repeat(repeated_indices, 2)

        # stack and reshape to get start - end pairs for each tile / subset
        stream_frame_start_stop_indices = \
            np.hstack([[0], multiplied_indices, [last_split_index]]).reshape(-1, 2)

        return stream_frame_start_stop_indices

    def set_frame_split_params(self):
        # stream_frame_dimension, model_frame_dimension
        self.horizontal_stream_frame_split_indices = \
            self._get_frame_split_indices(self.stream_frame_size[1], self.model_input_size[1])

        self.vertical_stream_frame_split_indices = \
            self._get_frame_split_indices(self.stream_frame_size[0], self.model_input_size[0])

        self.xy_split_ranges = [
            (h_range.tolist(), v_range.tolist())
            for h_range in self.horizontal_stream_frame_split_indices
            for v_range in self.vertical_stream_frame_split_indices]

        for el in self.xy_split_ranges:
            print(el)

        # get number of full 512 x 512 tiles which fit into the stream frame
        # get number of full 512 x 512 tiles which fit into the stream frame

    def split_stream_frame(self, frame):
        # tiles_count =

        pass

    def predict(self, frame):
        return self.model.predict(frame)


if __name__ == '__main__':
    # TODO camera code
    # src = 'rtmp://192.168.54.60:1935/live'
    # threaded_camera = ThreadedCamera(src, update_interval_seconds=0.5)
    # while True:
    #     try:
    #         threaded_camera.show_frame()
    #     except AttributeError:
    #         pass

    # TODO frame splitter validation
    sp = StreamProcessor()
    print('### sp.horizontal_stream_frame_split_indices ###')
    print(sp.horizontal_stream_frame_split_indices)

    print('### sp.vertical_stream_frame_split_indices ###')
    print(sp.vertical_stream_frame_split_indices)



# if __name__ == '__main__':
#     # address = "rtmp://myip:1935/myapp/mystream"
#     rtmp_url = "rtmp://192.168.54.60:1935/live"
#     cap = cv2.VideoCapture(rtmp_url)
#     cap.set(cv2.CAP_PROP_BUFFERSIZE, 2)
#     # print(f'FPS: {fps}')
#     # raise
#     # for el in dir(cap):
#     #     print(el)
#
#     counter = 0
#     while counter <= 20:
#         print(f'Frame: {counter}')
#         # cv2.destroyAllWindows()
#         read_flag, frame = cap.read()
#         cv2.imshow('frame', frame)
#         cv2.waitKey(1)
#         sleep(2)
#
#         counter += 1





