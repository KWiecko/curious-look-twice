import numpy as np

from transformations import repair_overlapping_labels


def test_cross_overlapping_3_classes():
    class_label_pattern = np.zeros((9, 9))
    # vertical line
    class_0_pattern = class_label_pattern.copy()
    class_0_pattern[:, 4] = 255
    class_1_pattern = class_label_pattern.copy()
    class_1_pattern[4, :] = 255
    class_2_pattern = class_label_pattern.copy()
    class_2_pattern[list(range(9)), list(range(9))] = 255

    stacked_labels = np.stack([class_0_pattern, class_1_pattern, class_2_pattern], axis=-1)
    max_class_channel_value = 255
    repaired_labels, _ = \
        repair_overlapping_labels(stacked_labels, max_class_channel_value=max_class_channel_value)

    expected_output = stacked_labels.copy() / 255
    for i in range(3):
        expected_output[:, :, i] *= (i + 1)
    expected_output[4, 4, 0] = 0
    expected_output[4, 4, 1] = 0
    np.testing.assert_array_equal(repaired_labels, expected_output)


def test_same_overlapping_3_classes():
    class_label_pattern = np.zeros((9, 9))
    # vertical line
    class_0_pattern = class_label_pattern.copy()
    class_0_pattern[0, :] = 255
    class_1_pattern = class_label_pattern.copy()
    class_1_pattern[0, :] = 255
    class_2_pattern = class_label_pattern.copy()
    class_2_pattern[0, :] = 255

    stacked_labels = np.stack([class_0_pattern, class_1_pattern, class_2_pattern], axis=-1)
    max_class_channel_value = 255
    repaired_labels, _ = \
        repair_overlapping_labels(stacked_labels, max_class_channel_value=max_class_channel_value)

    expected_output = stacked_labels.copy() / 255
    expected_output[:, :, :2] = 0
    expected_output[:, :, 2] *= 3

    np.testing.assert_array_equal(repaired_labels, expected_output)


def test_cross_overlapping_3_classes_flatten():
    class_label_pattern = np.zeros((9, 9))
    # vertical line
    class_0_pattern = class_label_pattern.copy()
    class_0_pattern[:, 4] = 255
    class_1_pattern = class_label_pattern.copy()
    class_1_pattern[4, :] = 255
    class_2_pattern = class_label_pattern.copy()
    class_2_pattern[list(range(9)), list(range(9))] = 255

    stacked_labels = np.stack([class_0_pattern, class_1_pattern, class_2_pattern], axis=-1)
    max_class_channel_value = 255
    repaired_labels, _ = \
        repair_overlapping_labels(
            stacked_labels, max_class_channel_value=max_class_channel_value, flatten_result=True)

    expected_output = stacked_labels.copy() / 255
    for i in range(3):
        expected_output[:, :, i] *= (i + 1)
    expected_output[4, 4, 0] = 0
    expected_output[4, 4, 1] = 0
    expected_output = np.sum(expected_output, axis=-1)

    np.testing.assert_array_equal(repaired_labels, expected_output)


def test_same_overlapping_3_classes_flatten():
    class_label_pattern = np.zeros((9, 9))
    # vertical line
    class_0_pattern = class_label_pattern.copy()
    class_0_pattern[0, :] = 255
    class_1_pattern = class_label_pattern.copy()
    class_1_pattern[0, :] = 255
    class_2_pattern = class_label_pattern.copy()
    class_2_pattern[0, :] = 255

    stacked_labels = np.stack([class_0_pattern, class_1_pattern, class_2_pattern], axis=-1)
    max_class_channel_value = 255
    repaired_labels, _ = \
        repair_overlapping_labels(
            stacked_labels, max_class_channel_value=max_class_channel_value,
            flatten_result=True)

    expected_output = np.zeros(class_2_pattern.shape)
    expected_output[0, :] = 3

    np.testing.assert_array_equal(repaired_labels, expected_output)
