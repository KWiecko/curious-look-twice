import numpy as np
import tensorflow as tf

from fpn.encoders import IdentityResBlock


def test_identity_block_0():
    test_input = tf.random.normal((1, 10, 10, 128))

    counts = [128, 128, 128]
    sizes = [1, 3, 1]
    block_id = 0

    identity_block = IdentityResBlock(counts, sizes, block_id)

    input_layer = tf.keras.layers.Input(shape=(10, 10, 128))
    model = tf.keras.models.Sequential([input_layer, identity_block])

    optimizer = tf.keras.optimizers.Adam()
    model.compile(optimizer=optimizer, loss='sparse_categorical_entropy',
                  metrics=['accuracy'])

    test_output = model(test_input)
    np.testing.assert_array_equal(test_input.shape, test_output.shape)


def test_identity_block_1():
    test_input = tf.random.normal((1, 10, 10, 128))

    counts = [128, 128, 128]
    sizes = [3, 3, 2]
    block_id = 0

    identity_block = IdentityResBlock(counts, sizes, block_id)

    input_layer = tf.keras.layers.Input(shape=(10, 10, 128))
    model = tf.keras.models.Sequential([input_layer, identity_block])

    optimizer = tf.keras.optimizers.Adam()
    model.compile(optimizer=optimizer, loss='sparse_categorical_entropy',
                  metrics=['accuracy'])

    test_output = model(test_input)
    np.testing.assert_array_equal(test_input.shape, test_output.shape)
