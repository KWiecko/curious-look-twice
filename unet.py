import tensorflow as tf

import model_config as mcfg
# 2 inputs RGB and NIR


def get_conv2d_stack(
        input_tensor, n_filters, stack_height=2, kernel_size=(3, 3),
        padding='same', activation='relu'):
    """
    Get stack of 2D convolution layers

    Parameters
    ----------
    input_tensor
    n_filters
    stack_height
    kernel_size
    padding
    activation

    Returns
    -------

    """
    x = input_tensor
    # get as many Conv2D layers as needed
    for i in range(stack_height):
        x = tf.keras.layers.Conv2D(
            filters=n_filters, kernel_size=kernel_size, kernel_initializer='he_normal',
            padding=padding)(x)
        # relu or leaky relu might be best but they are not supported with cuda?
        x = tf.keras.layers.Activation(activation)(x)

    return x


def get_conv2d_with_semi_inception_stack(
        input_tensor, n_filters, kernel_sizes=[3, 5, 7], activation='relu'):

    sub_blocks_count = len(kernel_sizes) + 1
    assert n_filters % sub_blocks_count == 0
    atomic_n_filters = int(n_filters / sub_blocks_count)
    # 1x1 conv no prior pooling
    conv_1x1 = \
        tf.keras.layers.Conv2D(
            filters=atomic_n_filters, kernel_size=(1, 1), padding='same',
            activation=activation)(input_tensor)
    sub_blocks = [conv_1x1]

    for k in kernel_sizes:
        curr_conv_1x1 = \
            tf.keras.layers.Conv2D(
                filters=atomic_n_filters, kernel_size=(1, 1), padding='same',
                activation=activation)(input_tensor)
        curr_nxn_conv = \
            tf.keras.layers.Conv2D(
                filters=atomic_n_filters, kernel_size=(k, k), padding='same',
                activation=activation)(curr_conv_1x1)
        sub_blocks.append(curr_nxn_conv)

    return tf.keras.layers.concatenate(sub_blocks)


def get_encoding_block(
        input_tensor, n_filters=64, conv_stack_height=2, conv_kernel_size=(3, 3),
        conv_padding='same', conv_activation='relu', pool_size=(2, 2), dropout=0.3,
        use_inceptions: bool = False):
    """
    conv2D block -> dropout -> pooling

    Parameters
    ----------
    input_tensor
    n_filters
    conv_kernel_size
    pool_size
    dropout

    Returns
    -------

    """
    # get 2D convolutions for current encoding step
    if use_inceptions:
        conv_output = \
            get_conv2d_with_semi_inception_stack(input_tensor=input_tensor, n_filters=n_filters)
    else:
        conv_output = get_conv2d_stack(
            input_tensor=input_tensor, n_filters=n_filters, stack_height=conv_stack_height,
            kernel_size=conv_kernel_size, padding=conv_padding, activation=conv_activation)
    # max pool to reduce dims
    pooling_output = tf.keras.layers.MaxPooling2D(pool_size=pool_size)(conv_output)
    # apply regularization via dropout
    encoding_output = tf.keras.layers.Dropout(dropout)(pooling_output)
    return conv_output, encoding_output


def get_encoder(input_tensor, conv2d_activation='relu', use_inceptions: bool = False):
    """
    Get whole encoder for unet

    Returns
    -------

    """
    # skip connectoins are needed -> enco
    conv_output_1, encoding_output_1 = get_encoding_block(
        input_tensor=input_tensor, n_filters=64, conv_activation=conv2d_activation,
        use_inceptions=use_inceptions)
    conv_output_2, encoding_output_2 = get_encoding_block(
        input_tensor=encoding_output_1, n_filters=128, conv_activation=conv2d_activation,
        use_inceptions=use_inceptions)
    conv_output_3, encoding_output_3 = get_encoding_block(
        input_tensor=encoding_output_2, n_filters=256, conv_activation=conv2d_activation,
        use_inceptions=use_inceptions)
    conv_output_4, encoding_output_4 = get_encoding_block(
        input_tensor=encoding_output_3, n_filters=512, conv_activation=conv2d_activation)

    return encoding_output_4, (conv_output_1, conv_output_2, conv_output_3, conv_output_4)
    # return encoding_output_3, (conv_output_1, conv_output_2, conv_output_3)
    # return encoding_output_2, (conv_output_1, conv_output_2)


def get_bottleneck(input_tensor, n_filters=512, bottleneck_activation='relu'):
    """
    'bridge' between encoder and decoder

    Parameters
    ----------
    input_tensor
    n_filters

    Returns
    -------

    """
    bottleneck = get_conv2d_stack(
        input_tensor=input_tensor, n_filters=n_filters, activation=bottleneck_activation)
    return bottleneck


# inputs, conv_output, n_filters=64, kernel_size=3, strides=3, dropout=0.3
def get_decoding_block(
        input_tensor, skip_tensor, n_filters=64, kernel_size=(3, 3),
        convt_strides=(2, 2), convt_padding='same', convt_dropout=0.3,
        conv2d_activation='relu'):
    # decoonv input
    deconv_output = tf.keras.layers.Conv2DTranspose(
        n_filters, kernel_size=kernel_size, strides=convt_strides,
        padding=convt_padding)(input_tensor)
    # concat skip connection and deconv input
    concat_tensors = tf.keras.layers.concatenate([deconv_output, skip_tensor])
    #  apply regularization
    dropout_output = tf.keras.layers.Dropout(convt_dropout)(concat_tensors)
    # apply 2d convolution
    conv_output = get_conv2d_stack(
        dropout_output, n_filters=n_filters, kernel_size=kernel_size,
        activation=conv2d_activation)
    return conv_output


# inputs, convs, output_channels
def get_decoder(input_tensor, conv_outputs, classes_count, conv2d_activation='relu'):
    """
    build decoder of unet

    Parameters
    ----------
    input_tensor: tf.Variable
        bottleneck output
    conv_outputs
    classes_count

    Returns
    -------

    """

    conv_output_1, conv_output_2, conv_output_3, conv_output_4 = conv_outputs
    # conv_output_1, conv_output_2, conv_output_3 = conv_outputs
    # conv_output_1, conv_output_2 = conv_outputs

    decode_output_4 = get_decoding_block(
        input_tensor=input_tensor, skip_tensor=conv_output_4, n_filters=512,
        kernel_size=(3, 3), convt_strides=(2, 2), conv2d_activation=conv2d_activation)

    decode_output_3 = get_decoding_block(
        input_tensor=decode_output_4, skip_tensor=conv_output_3, n_filters=256,
        kernel_size=(3, 3), convt_strides=(2, 2), conv2d_activation=conv2d_activation)

    decode_output_2 = get_decoding_block(
        input_tensor=decode_output_3, skip_tensor=conv_output_2, n_filters=128,
        kernel_size=(3, 3), convt_strides=(2, 2), conv2d_activation=conv2d_activation)

    decode_output_1 = get_decoding_block(
        input_tensor=decode_output_2, skip_tensor=conv_output_1, n_filters=64,
        kernel_size=(3, 3), convt_strides=(2, 2), conv2d_activation=conv2d_activation)

    # # decode_output_3 = get_decoding_block(
    # #     input_tensor=input_tensor, skip_tensor=conv_output_3, n_filters=32,
    # #     kernel_size=(3, 3), convt_strides=(2, 2), conv2d_activation=conv2d_activation)
    #
    # decode_output_2 = get_decoding_block(
    #     input_tensor=input_tensor, skip_tensor=conv_output_2, n_filters=256,
    #     kernel_size=(3, 3), convt_strides=(2, 2), conv2d_activation=conv2d_activation)
    #
    # decode_output_1 = get_decoding_block(
    #     input_tensor=decode_output_2, skip_tensor=conv_output_1, n_filters=128,
    #     kernel_size=(3, 3), convt_strides=(2, 2), conv2d_activation=conv2d_activation)

    outputs = tf.keras.layers.Conv2D(classes_count, (1, 1), activation='softmax')(decode_output_1)
    return outputs


def get_unet(classes_count, conv2d_activation='relu', use_inceptions: bool = False):

    # input has RGB and NIR channels concatenated
    model_input = tf.keras.layers.Input(shape=mcfg.EXPECTED_RGB_NIR_IMAGE_SHAPE)
    encoder_output, conv_outputs = \
        get_encoder(
            input_tensor=model_input, conv2d_activation=conv2d_activation,
            use_inceptions=use_inceptions)

    # bottle_neck = \
    #     get_bottleneck(
    #         input_tensor=encoder_output, n_filters=512, bottleneck_activation=conv2d_activation)

    bottle_neck = \
        get_bottleneck(
            input_tensor=encoder_output, n_filters=1024,
            bottleneck_activation=conv2d_activation)

    decoder_output = get_decoder(
        input_tensor=bottle_neck, conv_outputs=conv_outputs, classes_count=classes_count,
        conv2d_activation=conv2d_activation)
    model = tf.keras.models.Model(inputs=model_input, outputs=decoder_output)
    return model
