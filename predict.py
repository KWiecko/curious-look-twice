import numpy as np
from focal_loss import SparseCategoricalFocalLoss
import tensorflow as tf

from fpn.models import FullFPN
import model_config as mcfg
from transformations import generate_rgb_nir_concat_and_label_for_set, \
    get_observation_images, save_observation_plot
from unet import get_unet
from visualization import save_egb_label_pred

UNET = False
FPN = True


if __name__ == '__main__':

    if UNET:

        unet_model = get_unet(
            classes_count=mcfg.N_CLASSES, conv2d_activation='leaky_relu', use_inceptions=False)
        optimizer = tf.keras.optimizers.legacy.Adam(learning_rate=1e-3, amsgrad=True)
        # optimizer = tf.keras.optimizers.SGD(learning_rate=1e-2, momentum=0.9)

        unet_model.compile(
            optimizer=optimizer, loss='sparse_categorical_crossentropy', metrics=['accuracy'])

        latest_checkpoint = tf.train.latest_checkpoint('07_06_2022_checkpoints_best')
        unet_model.load_weights(latest_checkpoint)
        model = unet_model

    elif FPN:
        image_size = mcfg.EXPECTED_RGB_NIR_IMAGE_SHAPE[0]
        n_channels = mcfg.EXPECTED_RGB_NIR_IMAGE_SHAPE[-1]
        res_blocks_base_counts: tuple = (64, 128, 256, 512)
        fpn_model = FullFPN(
            image_size=image_size, n_channels=n_channels,
            n_classes=mcfg.N_CLASSES, res_blocks_base_counts=res_blocks_base_counts)
        fpn_model.build(input_shape=(None, image_size, image_size, n_channels))
        optimizer = tf.keras.optimizers.SGD(
            learning_rate=1e-2)  # , momentum=0.9)

        focal_loss = SparseCategoricalFocalLoss(
            gamma=2,
            class_weight=tf.constant(np.array([0.25] + [1.0 for _ in range(9)]),
                                     dtype=tf.float32))
        fpn_model.compile(optimizer=optimizer, loss=focal_loss,
                          metrics=['accuracy'])
        latest_checkpoint = tf.train.latest_checkpoint('checkpoints')
        fpn_model.load_weights(latest_checkpoint)
        model = fpn_model

    counter = 0

    for pred_idx, (rgb, label) in enumerate(
            generate_rgb_nir_concat_and_label_for_set('validation', concat_rgb_nir=False)):

        rgb = tf.expand_dims(rgb, axis=0)
        label = tf.expand_dims(label, axis=0)
        # eval = unet_model.evaluate(rgb, label, verbose=2)
        # print(eval)
        pred = model.predict(rgb)
        pred_flat = np.argmax(pred[0], axis=-1)

        # obs_tuple = get_observation_images(oin, classes=classes, scale_label=True)
        filler = np.zeros(pred_flat.shape)
        saved_label = np.stack([(label.numpy()[0].reshape(pred_flat.shape) * 255 / 9), filler, filler], axis=-1).astype('uint8')
        saved_pred = np.stack([(pred_flat * 255 / 9), filler, filler], axis=-1).astype('uint8')
        fname = f'pred_{pred_idx}.png'
        save_egb_label_pred(fname, (rgb.numpy()[0] * 255., saved_label, saved_pred))
        counter += 1
        if counter > 500:
            break
