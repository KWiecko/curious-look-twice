import numpy as np
import tensorflow as tf


def mean_iou_loss(y_true, y_pred):
    # get number of classes
    num_classes = y_pred.shape[-1]
    classes_preds_batch = tf.argmax(y_pred[:], axis=-1)
    labels_2d_batch = tf.cast(y_true[:, :, :, 0], classes_preds_batch.dtype)
    # true_positives = tf.equal(classes_preds_batch, labels_2d_batch)
    true_positives = classes_preds_batch == labels_2d_batch

    intersection_array = tf.zeros((num_classes,))
    intersection_class_indices, _intersection_index, intersection_areas = \
        tf.unique_with_counts(labels_2d_batch[true_positives][:])
    intersection_array = tf.tensor_scatter_nd_update(
        intersection_array,
        tf.reshape(intersection_class_indices, (-1, 1)),
        tf.cast(intersection_areas, intersection_array.dtype))

    # for each class sum all pixels on both predictions and labels and
    # subtract intersection value to get union
    preds_class_indices, _preds_index, preds_class_areas = tf.unique_with_counts(tf.reshape(classes_preds_batch, (-1,)))
    label_class_indices, _label_index, label_class_areas = tf.unique_with_counts(tf.reshape(labels_2d_batch, (-1,)))

    union_array = tf.zeros((num_classes,))
    # call update again
    union_array = tf.tensor_scatter_nd_update(
        union_array,
        tf.reshape(preds_class_indices, (-1, 1)),
        tf.cast(preds_class_areas, union_array.dtype))

    # call update again
    union_array = union_array + tf.tensor_scatter_nd_update(
        tf.zeros((num_classes,)),
        tf.reshape(label_class_indices, (-1, 1)),
        tf.cast(label_class_areas, union_array.dtype))

    # call update again
    union_array -= intersection_array
    iou_with_nans = intersection_array / union_array
    is_iou_nan = tf.math.is_nan(iou_with_nans)
    iou_no_nans = tf.boolean_mask(iou_with_nans, ~is_iou_nan)
    return iou_no_nans


class MeanIoULoss(tf.keras.losses.Loss):
    def __init__(self, name='mean_iou_loss'):
        super().__init__(name=name)

    def call(self, y_true, y_pred):
        iou_array = mean_iou_loss(y_true, y_pred)
        return 1 - tf.reduce_mean(iou_array)


def mean_iou_loss_np(preds, labels):
    num_classes = preds.shape[-1]
    # assume shape (batch, height, width, num_classes)
    classes_preds_batch = np.argmax(preds[:], axis=-1)
    labels_2d_batch = labels[:, :, :, 0]
    intersection_flags = classes_preds_batch == labels_2d_batch

    # print('### true_positives.shape ###')
    # print(labels_2d_batch[intersection_flags][:])

    intersection_array = np.zeros((num_classes,))
    # select only those values in label where predictions mach
    # intersection = labels_2d_batch[intersection_flags]
    intersection_class_indices, intersection_areas = \
        np.unique(labels_2d_batch[intersection_flags][:], return_counts=True)
    # print(intersection_class_indices)
    # print(intersection_areas)
    # raise
    intersection_array[intersection_class_indices] = intersection_areas

    # for each class sum all pixels on both predictions and labels and
    # subtract intersection value to get union
    preds_class_indices, preds_class_areas = np.unique(classes_preds_batch, return_counts=True)
    label_class_indices, label_class_areas = np.unique(labels_2d_batch, return_counts=True)

    union_array = np.zeros((num_classes,))
    union_array[preds_class_indices] += preds_class_areas
    union_array[label_class_indices] += label_class_areas
    union_array -= intersection_array
    # just to make this not nan
    # union_array[union_array == 0] = 1
    print(intersection_array / union_array)
    return 1 / np.nanmean(intersection_array / union_array)


if __name__ == '__main__':
    np.set_printoptions(linewidth=np.inf)
    np.random.seed(0)
    preds = np.zeros((10, 10, 5))

    # |----------|----------|
    # |    0     |     2    |
    # | -------- | -------- |
    # |    1     |     1    |
    # |----------|----------|

    preds[:5, :5, 0] = 0.9
    preds[:5, :5, 1] = 0.1
    preds[:5, :5, 2] = 0.0

    preds[:5, 5:, 0] = 0.1
    preds[:5, 5:, 1] = 0.1
    preds[:5, 5:, 2] = 0.8

    preds[5:, 5:, 0] = 0.1
    preds[5:, 5:, 1] = 0.9
    preds[5:, 5:, 2] = 0.0

    preds[5:, :5, 0] = 0.1
    preds[5:, :5, 1] = 0.8
    preds[5:, :5, 2] = 0.1

    print(preds[:, :, 0])
    print()
    print(preds[:, :, 1])
    print()
    print(preds[:, :, 2])

    label = np.random.randint(0, 3, (10, 10, 1))
    label[:4, :4, 0] = 0
    label[:4, 6:, 0] = 2

    label[6:, :4, 0] = 1
    label[6:, 6:, 0] = 1

    label[4, :, 0] = 1
    label[5, :, 0] = 2

    label[:4, 4, 0] = 2
    label[:4, 5, 0] = 1

    label[6:, 4, 0] = 0
    label[6:, 5, 0] = 0

    print(label[:, :, 0])
    print('### label.shape ###')
    print(label.shape)
    print(tf.one_hot(label[:, :, 0], 5).numpy().shape)
    # raise

    # |----------|----------|
    # |    0     |     2    |
    # | -------- | -------- |
    # |    1     |     1    |
    # |----------|----------|

    preds1 = preds.copy()
    preds1[:, :, :] = 0
    preds1[:, :, 0] = 1.0
    label1 = label.copy()
    label1[:, :, :] = 0

    preds = np.array([preds, preds1])
    labels = np.array([label, label1])
    # print(preds.shape)
    # print(labels.shape)
    # raise

    # preds = preds.reshape((1, 10, 10, 5))
    # label = label.reshape((1, 10, 10, 1))
    res = mean_iou_loss_np(preds, labels)
    print(1 / res)

    iou = tf.keras.metrics.IoU(
        num_classes=5,
        target_class_ids=list(range(5)),
        sparse_y_true=True,
        sparse_y_pred=False)

    # y_true, y_pred
    # preds = preds.reshape((1, 10, 10, 5))
    # label = label.reshape((1, 10, 10, 1))
    iou.update_state(labels, preds)
    res = iou.result().numpy()
    print(res)

    # y_true, y_pred
    res1 = mean_iou_loss(labels, preds)
    print(np.float32(1 / res1.numpy()))

