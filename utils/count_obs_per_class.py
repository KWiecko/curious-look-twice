import cv2
import json
import numpy as np
import os
from tqdm import tqdm

from paths import BASE_PATH, TRAIN_LABELS_PATH, VAL_LABELS_PATH

if __name__ == '__main__':

    print(os.sep.join([BASE_PATH, TRAIN_LABELS_PATH]))

    label_names = os.listdir(os.sep.join([BASE_PATH, TRAIN_LABELS_PATH]))

    class_counter = {label: 0 for label in label_names}
    class_list = {label: [] for label in label_names}

    for case, case_path in zip(['train', 'validation'], [TRAIN_LABELS_PATH, VAL_LABELS_PATH]):
        for label in label_names:

            # get all files for label
            filenames = os.listdir(os.sep.join([BASE_PATH, case_path, label]))

            for filename in tqdm(filenames):
                img_path = os.sep.join([BASE_PATH, case_path, label, filename])
                image = cv2.imread(os.sep.join([BASE_PATH, case_path, label, filename]))

                ch0_sum = image[:, :, 0].sum()
                ch1_sum = image[:, :, 1].sum()
                ch2_sum = image[:, :, 2].sum()

                assert ch0_sum == ch1_sum == ch2_sum

                if ch0_sum != 0:
                    class_counter[label] += 1
                    class_list[label].append(filename.replace('.png', '.jpg'))

            print(class_counter)

        # save counted files
        with open(f'{case}_class_filenames.json', 'w') as cff:
            class_list_json = json.dumps(class_list)
            cff.write(class_list_json)
