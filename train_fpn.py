import datetime
import numpy as np
import tensorflow as tf
import tensorflow_addons as tfa
# from callbacks import TB

from focal_loss import SparseCategoricalFocalLoss
from transformations import generate_rgb_nir_concat_and_label_for_set, get_gen_size
from fpn.models import FullFPN
# from losses import MeanIoULoss
import model_config as mcfg
# from unet import get_unet


START_FROM_CHECKPOINT = True


if __name__ == '__main__':

    gpus = tf.config.experimental.list_physical_devices('GPU')
    for gpu in gpus:
        tf.config.experimental.set_memory_growth(gpu, True)

    # train_samples_count = get_gen_size('train')
    test_samples_count = get_gen_size('test')
    validation_samples_count = get_gen_size('validation')

    # {'drydown': 16806, 'weed_cluster': 11111, 'planter_skip': 2599, 'endrow': 4481, 'nutrient_deficiency': 13308, 'storm_damage': 356, 'water': 2155, 'double_plant': 6234, 'waterway': 3899}
    train_dataset = tf.data.Dataset.from_generator(
        generator=generate_rgb_nir_concat_and_label_for_set,
        args=('train', mcfg.CONCAT_RGB_NIR, mcfg.TRAIN_SAMPLES_COUNT),
        output_types=(tf.float32, tf.int32),
        output_shapes=(mcfg.EXPECTED_RGB_NIR_IMAGE_SHAPE, mcfg.EXPECTED_LABEL_SHAPE))\
        .shuffle(mcfg.BUFFER_SIZE).batch(mcfg.BATCH_SIZE).repeat()\
        .prefetch(buffer_size=tf.data.experimental.AUTOTUNE)

    # # this is to check dataset
    # sample = train_dataset.take(1)
    # sample_numpy = [el for el in sample.as_numpy_iterator()]
    # # print(sample_numpy.shape)
    # print(sample_numpy[0][0].shape)
    # print(sample_numpy[0][1].shape)
    # print(sample_numpy[1][0].shape)
    # print(sample_numpy[1][1].shape)

    # # TODO test set will be used for testing only
    # test_dataset = tf.data.Dataset.from_generator(
    #     generator=generate_rgb_nir_concat_and_label_for_set, args=('test',),
    #     output_types=(tf.float32, tf.int32),
    #     output_shapes=(mcfg.EXPECTED_RGB_NIR_IMAGE_SHAPE, mcfg.EXPECTED_LABEL_SHAPE))

    valid_dataset = tf.data.Dataset.from_generator(
        generator=generate_rgb_nir_concat_and_label_for_set,
        args=('validation', mcfg.CONCAT_RGB_NIR, mcfg.VAL_SAMPLES_COUNT),
        output_types=(tf.float32, tf.int32),
        output_shapes=(mcfg.EXPECTED_RGB_NIR_IMAGE_SHAPE, mcfg.EXPECTED_LABEL_SHAPE))\
        .batch(mcfg.BATCH_SIZE)

    # unet_model = get_unet(
    #     classes_count=mcfg.N_CLASSES, conv2d_activation='leaky_relu', use_inceptions=False)

    image_size = mcfg.EXPECTED_RGB_NIR_IMAGE_SHAPE[0]
    n_channels = mcfg.EXPECTED_RGB_NIR_IMAGE_SHAPE[-1]

    res_blocks_base_counts: tuple = (64, 128, 256, 512)
    fpn_model = FullFPN(
        image_size=image_size, n_channels=n_channels, n_classes=mcfg.N_CLASSES,
        res_blocks_base_counts=res_blocks_base_counts)
    fpn_model.build(input_shape=(None, image_size, image_size, n_channels))

    print('### FPN-SEG SUMMARY ###')
    print(fpn_model.summary())

    # optimizer = tf.keras.optimizers.Adam(learning_rate=5e-3)  # , amsgrad=False)
    # optimizer = tf.keras.optimizers.Adam(learning_rate=1e-3)  # , amsgrad=False)
    # optimizer = tf.keras.optimizers.Adam(
    #     tf.keras.optimizers.schedules.PolynomialDecay(
    #         initial_learning_rate=1e-2, decay_steps=2000,
    #         end_learning_rate=2.5e-5))
    # optimizer = tf.keras.optimizers.SGD(learning_rate=1e-4)  # , momentum=0.9)
    optimizer = tf.keras.optimizers.Adam(learning_rate=5e-3)  # , momentum=0.9)

    focal_loss = SparseCategoricalFocalLoss(
        gamma=2, class_weight=tf.constant(np.array([0.25] + [1.0 for _ in range(9)]), dtype=tf.float32))
    fpn_model.compile(optimizer=optimizer, loss=focal_loss, metrics=['accuracy'])

    if START_FROM_CHECKPOINT:
        # TODO make sure that wieghts fit the model
        latest_checkpoint = tf.train.latest_checkpoint('checkpoints')
        # latest_checkpoint = tf.train.latest_checkpoint('07_06_2022_checkpoints_best')
        # latest_checkpoint = tf.train.latest_checkpoint('03_10_2022_checkpoints_best')
        if latest_checkpoint:
            fpn_model.load_weights(latest_checkpoint)

    # loss, acc = unet_model.evaluate(valid_dataset, verbose=2)
    # print(f'LOSS: {loss} | ACC: {acc}')

    # unet_model.save('models/best_model')

    # TODO uncomment
    steps_per_epoch = mcfg.TRAIN_SAMPLES_COUNT // mcfg.BATCH_SIZE

    # tODO check when per-batch dump will be fixed in TensorBoard
    # create monitoring callback
    # log_dir = "logs/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    # tensor_board_callback = tf.keras.callbacks.TensorBoard(
    #     log_dir=log_dir, histogram_freq=1, update_freq='batch')
    # tensor_board_callback = TB(log_dir=log_dir, histogram_freq=1, update_freq=10)

    # model save (every ~2 hours)
    todays_date = str(datetime.datetime.now().date()).replace('-', '').replace(' ', '')
    model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
        filepath=f'checkpoints/{todays_date}-fpn-seg',
        save_weights_only=True,
        monitor='train_loss',
        mode='min',
        save_freq='epoch',
        save_best_only=True)

    # TODO re-enable checkpointing afterwards?
    fpn_model.fit(
        train_dataset, epochs=500, steps_per_epoch=steps_per_epoch,
        validation_data=valid_dataset, callbacks=[model_checkpoint_callback])

