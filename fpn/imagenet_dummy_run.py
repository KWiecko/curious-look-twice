from pathlib import Path

import tensorflow as tf
import tensorflow_datasets as tfds

from encoders import ResNet50


IMAGENET_DATASET_CACHE = Path('/home/kw/Projects/drone_cv/curious-look-twice/tests/datasets')
BUFFER_SIZE = 8
BATCH_SIZE = 4

if __name__ == '__main__':

    (train_ds, test_ds), info = tfds.load(
        'mnist', data_dir=IMAGENET_DATASET_CACHE / 'stanford_dogs',
        split=['train', 'test'], shuffle_files=True, as_supervised=True, with_info=True)

    # rescale to 224x224
    train_ds = train_ds.map(lambda x, y: (tf.image.resize(x, (128, 128)), y))\
        .shuffle(BUFFER_SIZE).batch(BATCH_SIZE)\
        .prefetch(buffer_size=tf.data.experimental.AUTOTUNE)

    test_ds = test_ds.map(lambda x, y: (tf.image.resize(x, (128, 128)), y)) \
        .shuffle(BUFFER_SIZE).batch(BATCH_SIZE)\
        .prefetch(buffer_size=tf.data.experimental.AUTOTUNE)

    # for image, label in train_ds.take(1):
    #     print(image.shape)
    #     print(label.numpy())

    resnet = ResNet50(
        res_blocks_base_counts=(32, 64, 128, 256), classifier=True, num_classes=10)
    resnet.build(input_shape=(None, 128, 128, 1))
    # print(resnet.summary())
    # raise
    optimizer = tf.keras.optimizers.Adam(learning_rate=1e-3)
    resnet.compile(
        optimizer=optimizer, loss='sparse_categorical_crossentropy', metrics=['accuracy'])
    resnet.fit(train_ds, epochs=100, validation_data=test_ds)
