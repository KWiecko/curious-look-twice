from typing import Union

import numpy as np
import tensorflow as tf


class IdentityResBlock(tf.keras.models.Model):
    def __init__(self, counts, sizes, block_id: Union[int, str] = 0, **kwargs):
        super(IdentityResBlock, self).__init__(**kwargs)

        self.zipped_block_params = list(zip(counts, sizes))
        assert len(self.zipped_block_params) == 3

        self.hidden_layers = []

        counter = 1
        for count, size in self.zipped_block_params[:-1]:
            # first N 'blocks' of identity block
            assert isinstance(size, int)
            self.hidden_layers += [
                # usually a 1x1 conv goes first followed by BN and RELU
                tf.keras.layers.Conv2D(
                    count, (size, size), padding='same',
                    kernel_initializer=tf.keras.initializers.glorot_uniform(seed=0),
                    name=f'identity_block_{block_id}_conv{size}x{size}_{counter}'),
                tf.keras.layers.BatchNormalization(name=f'identity_block_{block_id}_bn_{counter}'),
                tf.keras.layers.ReLU(name=f'identity_block_{block_id}_relu_{counter}')]
            counter += 1

        # add last layer with no activation -> activation comes after summation
        count, size = self.zipped_block_params[-1]
        self.hidden_layers += [
            tf.keras.layers.Conv2D(
                count, (size, size), padding='same', activation=None,
                kernel_initializer=tf.keras.initializers.glorot_uniform(seed=0),
                name=f'identity_block_{block_id}_conv{size}x{size}_{counter}'),
            tf.keras.layers.BatchNormalization(name=f'identity_block_{block_id}_bn_{counter}')]

        self.add_x_and_shortcut = tf.keras.layers.Add(name=f'identity_block_{block_id}_add')
        self.block_activation = tf.keras.layers.ReLU(name=f'identity_block_{block_id}_activation')

    def get_output(self):
        return self.block_activation.output

    def call(self, inputs, training=None, mask=None, *args, **kwargs):
        x = inputs
        x_shortcut = inputs

        for hidden_layer in self.hidden_layers:
            x = hidden_layer(x)

        # sum shortcut and x
        x = self.add_x_and_shortcut([x, x_shortcut])
        x = self.block_activation(x)
        return x


class ConvResBlock(tf.keras.models.Model):
    def __init__(self, counts, sizes, conv_stride, block_id: Union[int, str] = 0, **kwargs):
        super(ConvResBlock, self).__init__(**kwargs)

        self.zipped_block_params = list(zip(counts, sizes))

        self.hidden_layers = []

        self.conv_stride = conv_stride

        counter = 0

        for count, size in self.zipped_block_params[:-1]:
            strides = self.conv_stride if counter == 0 else 1
            padding = 'valid' if counter == 0 else 'same'
            self.hidden_layers += [
                tf.keras.layers.Conv2D(
                    count, (size, size), padding=padding, strides=strides,
                    kernel_initializer=tf.keras.initializers.glorot_uniform(seed=0),
                    name=f'conv_block_{block_id}_conv{size}x{size}_{counter}'),
                tf.keras.layers.BatchNormalization(name=f'conv_block_{block_id}_bn_{counter}'),
                tf.keras.layers.ReLU(name=f'conv_block_{block_id}_relu_{counter}')]

            counter += 1

        count, size = self.zipped_block_params[-1]
        self.hidden_layers += [
            tf.keras.layers.Conv2D(
                count, (size, size), padding='same', activation=None,
                kernel_initializer=tf.keras.initializers.glorot_uniform(seed=0),
                name=f'identity_block_{block_id}_conv{size}x{size}_{counter}'),
            tf.keras.layers.BatchNormalization(
                name=f'identity_block_{block_id}_bn_{counter}')]

        # now the shortcut with convolution
        # x, y dim reduction happens in the first conv layer of the main path (via strides)
        # number of filters between last layer of the main path and shortcut path must match
        _, shortcut_size = self.zipped_block_params[0]
        shortcut_count, _ = self.zipped_block_params[-1]
        self.shortcut_conv = \
            tf.keras.layers.Conv2D(
                shortcut_count, (shortcut_size, shortcut_size), strides=self.conv_stride,
                kernel_initializer=tf.keras.initializers.glorot_uniform(seed=0),
                padding='valid', name=f'conv_block_{block_id}_conv{size}x{size}_shortcut')
        self.shortcut_bn = tf.keras.layers.BatchNormalization(name=f'conv_block_{block_id}_bn_shortcut')

        # add main path and shortcut
        self.add_x_and_shortcut = tf.keras.layers.Add(name=f'conv_block_{block_id}_add')
        self.block_activation = tf.keras.layers.ReLU(name=f'conv_block_{block_id}_activation')

    def call(self, inputs, training=None, mask=None, *args, **kwargs):
        x = inputs
        x_shortcut = inputs
        # call all layers in main path
        for hidden_layer in self.hidden_layers:
            x = hidden_layer(x)

        # follow the shortcut path
        x_shortcut = self.shortcut_conv(x_shortcut)
        x_shortcut = self.shortcut_bn(x_shortcut)

        x = self.add_x_and_shortcut([x, x_shortcut])
        x = self.block_activation(x)
        # activate block
        return x


# todo this should inherit from model not Layer
class ResNet50(tf.keras.models.Model):

    @property
    def identity_blocks_counts(self):
        return {0: 2, 1: 3, 2: 5, 3: 2}

    def __init__(
            self, entry_layer_count=64, entry_layer_size=7, entry_layer_strides=2, entry_layer_pool_size=2,
            entry_layer_pool_strides=2, res_block_conv_stride=2, res_blocks_base_counts=(64, 128, 256, 512),
            res_blocks_base_sizes=(1, 3, 1), classifier: bool = False, avg_pool_size=2, num_classes=10, **kwargs):

        super(ResNet50, self).__init__(**kwargs)

        # make sure to scale down the input image

        # self.input_size = input_size
        self.entry_layer_count = entry_layer_count
        self.entry_layer_size = entry_layer_size
        self.entry_layer_strides = entry_layer_strides
        self.entry_layer_pool_size = entry_layer_pool_size
        self.entry_layer_pool_strides = entry_layer_pool_strides
        self._set_entry_block()
        # allow only 4 res blocks
        assert len(res_blocks_base_counts) == 4
        assert len(res_blocks_base_sizes) < 5

        self.res_blocks_base_counts = res_blocks_base_counts
        self.res_blocks_base_sizes = res_blocks_base_sizes

        assert res_block_conv_stride > 0
        self.res_block_conv_stride = res_block_conv_stride
        self._set_res_blocks()

        assert classifier is not None
        self.classifier = classifier
        # initialize but left None
        self.avg_pool_size = None
        self.num_classes = None

        if self.classifier:
            assert avg_pool_size > 1
            assert num_classes > 1

            self.avg_pool_size = avg_pool_size
            self.num_classes = num_classes
            self._add_classifier_head()

            # add dense and softmax head

    def get_input(self):
        return self.entry_zero_padding.input

    # def set_input(self, input_shape=(None, 512, 512, 3)):
    #     self.inputs = tf.keras.layers.Input(shape=input_shape)
    #
    # def set_outputs(self):
    #     if self.classifier:
    #         self.outputs = self.classif_softmax
    #     else:
    #         self.outputs = self.res_blocks[-1][-1]

    def _set_entry_block(self):
        entry_layer_size_tuple = (self.entry_layer_size, self.entry_layer_size)
        entry_padding_size = tuple(int(self.entry_layer_size / 2) for el in entry_layer_size_tuple)

        # TODO maybe parametrize this so padding can be calculated instead of
        #  hardocded
        self.entry_zero_padding = tf.keras.layers.ZeroPadding2D(entry_padding_size, name='entry_zero_padding')
        self.entry_conv = \
            tf.keras.layers.Conv2D(
                    self.entry_layer_count, entry_layer_size_tuple,
                    padding='valid', strides=self.entry_layer_strides,
                    kernel_initializer=tf.keras.initializers.glorot_uniform(seed=0),
                    name=f'entry_conv_block{self.entry_layer_size}x{self.entry_layer_size}')

        self.entry_bn = tf.keras.layers.BatchNormalization(name=f'entry_conv_block_bn')
        self.entry_relu = tf.keras.layers.ReLU(name=f'entry_conv_block_relu')

        pool_size = (self.entry_layer_pool_size, self.entry_layer_pool_size)
        if self.entry_layer_pool_size > 2:
            pool_padding_size = tuple(int(np.ceil((el - 2) / 2)) for el in pool_size)
            self.entry_before_max_pool_padding = \
                tf.keras.layers.ZeroPadding2D(pool_padding_size, name='entry_before_max_pool_padding')

        # pool_size=(2, 2), strides=None,
        self.entry_max_pool = \
            tf.keras.layers.MaxPool2D(
                pool_size=pool_size, strides=self.entry_layer_pool_strides, name='entry_conv_block_maxpool')

    def _set_res_blocks(self):
        self.res_blocks = []

        for block_index, block_base_count in enumerate(self.res_blocks_base_counts):

            block = []
            # counts, sizes, conv_stride, block_id
            block += [ConvResBlock(
                counts=[block_base_count, block_base_count, 2 * block_base_count],
                sizes=self.res_blocks_base_sizes, conv_stride=self.res_block_conv_stride,
                block_id=block_index, name=f'conv_res_block_{block_index}')]

            identity_blocks_count = self.identity_blocks_counts[block_index]

            # for el in range(identity_blocks_count):
            #     print(f'conv_block_id: {block_index} | ident_block_id: {block_index}{el}')

            block += [
                IdentityResBlock(
                    counts=[block_base_count, block_base_count, 2 * block_base_count],
                    sizes=self.res_blocks_base_sizes, block_id=f'{block_index}{el}',
                    name=f'identity_res_block_{block_index}{el}')
                for el in range(identity_blocks_count)]

            self.res_blocks.append(block)

    def _add_classifier_head(self):
        # average pool
        self.classif_avg_pool = \
            tf.keras.layers.AveragePooling2D((self.avg_pool_size, self.avg_pool_size), name='classif_block_avg_pool')
        # flatten
        self.classif_flatten = tf.keras.layers.Flatten(name='classif_block_flatten')
        # dense
        self.classif_dense = tf.keras.layers.Dense(self.num_classes, activation=None, name='classif_block_dense')
        # softmax
        self.classif_softmax = tf.keras.layers.Softmax(name='classif_block_softmax')

    def call(self, inputs, training=None, mask=None, *args, **kwargs):
        # x = self.input_layer(inputs)
        x = inputs
        x = self.entry_zero_padding(x)
        # print(f'after padding: {x.shape}')
        x = self.entry_conv(x)
        # print(f'after conv: {x.shape}')
        x = self.entry_bn(x)
        x = self.entry_relu(x)
        if self.entry_layer_pool_size > 2:
            x = self.entry_before_max_pool_padding(x)
        x = self.entry_max_pool(x)
        # print(f'after max_pool: {x.shape}')

        for res_block in self.res_blocks:
            for block in res_block:
                x = block(x)
            # print(print(f'after res block: {x.shape}'))

        if self.classifier:
            x = self.classif_avg_pool(x)
            x = self.classif_flatten(x)
            x = self.classif_dense(x)
            x = self.classif_softmax(x)
        return x


if __name__ == '__main__':
    test_input = tf.random.normal((1, 10, 10, 128))
    print(test_input.shape)

    counts = [128, 128, 128]
    sizes = [1, 3, 1]
    block_id = 0
    # k0 = 3
    # s0 = 2
    # W = 10
    # [(W−K+2P)/S]+1 = [(10 − 3) / 2] + 1 = 4.5

    # counts, sizes, conv_stride, block_id: int = 0
    identity_block_model = ConvResBlock(counts, sizes, conv_stride=2, block_id=0)

    input_layer = tf.keras.layers.Input(shape=(None, 512, 512, 3))

    resnet = ResNet50(res_blocks_base_counts=(16, 32, 64, 128), classifier=True)
    # resnet.set_input(input_shape=(None, 512, 512, 3))
    # resnet.set_outputs()
    # resnet(tf.random.normal((1, 512, 512, 3)))
    # model = tf.keras.models.Sequential([resnet])
    # (None, 10, 10, 128) -> None needed to satisfy batch dim
    resnet.build(input_shape=(None, 512, 512, 3))
    resnet.call(tf.keras.Input(shape=(512, 512, 3)))
    print(resnet.entry_zero_padding.input)
    print(resnet.get_layer('identity_res_block_01').outputs)
    # print(resnet.entry_zero_padding.input)

    optimizer = tf.keras.optimizers.Adam()
    resnet.compile(optimizer=optimizer, loss='sparse_categorical_entropy', metrics=['accuracy'])
    print(resnet.summary())

    # test_input = tf.random.normal((1, 512, 512, 3))
    # test_output = resnet(test_input)
    # print(test_output.shape)
    # print(test_output.numpy())
