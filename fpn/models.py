import tensorflow as tf

from fpn.encoders import ResNet50
from fpn.decoders import FullDecoder

DESIRED_RESNET_OUTPUT_NAMES = (
    'identity_res_block_01',
    'identity_res_block_12',
    'identity_res_block_24',
    'identity_res_block_31')

# TODO: based on article -> https://openaccess.thecvf.com/content_cvpr_2018_workshops/papers/w4/Seferbekov_Feature_Pyramid_Network_CVPR_2018_paper.pdf


class FullFPN(tf.keras.models.Model):
    def __init__(
            self, n_classes: int = 10, res_blocks_base_counts: tuple = (32, 64, 128, 256),
            res_blocks_base_sizes: tuple = (1, 3, 1),
            desired_resnet_output_names: tuple = None, image_size: int = 512,
            n_channels: int = 3):
        super(FullFPN, self).__init__()

        self.num_classes = n_classes

        # instantiate resnet without classifier
        self.resnet = ResNet50(
            res_blocks_base_counts=res_blocks_base_counts, res_blocks_base_sizes=res_blocks_base_sizes,
            num_classes=n_classes)

        self.resnet.build(input_shape=(None, image_size, image_size, n_channels))
        self.resnet.call(tf.keras.Input(shape=(image_size, image_size, n_channels)))

        if desired_resnet_output_names is None:
            desired_resnet_output_names = DESIRED_RESNET_OUTPUT_NAMES

        self.resnet_outputs = list(reversed([
            layer.output for layer in self.resnet.layers if layer.name in desired_resnet_output_names]))

        for layer_output in self.resnet_outputs:
            print(layer_output.shape)

        self.multi_output_resnet = tf.keras.models.Model(inputs=self.resnet.get_input(), outputs=self.resnet_outputs)
        # collect outputs from ends of identity blocks

        # instantiate decoder
        self.full_decoder = FullDecoder(num_classes=n_classes)
        decoder_input_shapes = [tuple(output.shape) for output in self.resnet_outputs]
        self.full_decoder.build(input_shape=decoder_input_shapes)

    def call(self, inputs, training=None, mask=None, *args, **kwargs):
        # encoder_outputs = self.resnet(inputs)
        encoder_outputs = self.multi_output_resnet(inputs)
        #
        # tODO maybe move prediction / softmax layer here?
        return self.full_decoder(encoder_outputs)


if __name__ == '__main__':

    image_size = 512
    n_channels = 3

    res_blocks_base_counts: tuple = (16, 32, 64, 128)

    fpn_seg = FullFPN(image_size=image_size, n_channels=n_channels)
    fpn_seg.build(input_shape=(None, image_size, image_size, n_channels))
    optimizer = tf.keras.optimizers.Adam(learning_rate=1e-3)
    fpn_seg.compile(optimizer=optimizer, loss='sparse_categorical_crossentropy', metrics=['accuracy'])

    print(fpn_seg.summary())
