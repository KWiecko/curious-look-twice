import numpy as np
import tensorflow as tf


class ConvAddUpsampleBlock(tf.keras.models.Model):

    def __init__(
            self, up_count: int = 256, up_size: int = 2, conv_output_size=3,
            conv_output_count: int = 128, conv_output_convs_count: int = 2, block_id: int = 0):
        super(ConvAddUpsampleBlock, self).__init__()
        # conv 1x1 from encoder is required
        self.up_count = up_count
        self.encoder_conv = tf.keras.layers.Conv2D(
            up_count, (1, 1), padding='same',
            kernel_initializer=tf.keras.initializers.glorot_uniform(seed=0),
            name=f'conv_add_upsample_block_{block_id}_conv1x1')
        self.encoder_bn = tf.keras.layers.BatchNormalization(name=f'conv_add_upsample_block_{block_id}_bn')
        self.encoder_relu = tf.keras.layers.ReLU(name=f'conv_add_upsample_block_{block_id}_relu')
        assert up_size > 1
        # if input from lower decoder block is available it needs to be upsampled
        self.up_size = up_size
        self.up_sum = tf.keras.layers.UpSampling2D(
            (self.up_size, self.up_size),
            name=f'conv_add_upsample_block_{block_id}_upsample{self.up_size}x{self.up_size}')
        # add conv 1x1 from encoder and upsampled input
        self.add_encoded_to_up = tf.keras.layers.Add(name=f'conv_add_upsample_block_{block_id}_add')

        # name=f'identity_block_{block_id}_conv{size}x{size}_{counter}')

        # 2 outputs are required:
        # - first one is 2x 3x3 conv with 'same' padding
        assert conv_output_count > 0
        assert conv_output_size > 0
        assert conv_output_convs_count > 0
        self.conv_output_count = conv_output_count
        self.conv_output_size = conv_output_size
        self.conv_output_convs_count = conv_output_convs_count

        self.conv_output_path = []

        for counter in range(conv_output_convs_count):
            self.conv_output_path += [
                tf.keras.layers.Conv2D(
                    self.conv_output_count, (self.conv_output_size, self.conv_output_size), padding='same',
                    kernel_initializer=tf.keras.initializers.glorot_uniform(seed=0),
                    name=f'conv_add_upsample_block_{block_id}_conv{self.conv_output_size}x{self.conv_output_size}_{counter}_output'),
                tf.keras.layers.BatchNormalization(name=f'conv_add_upsample_block_{block_id}_bn_{counter}_output'),
                tf.keras.layers.ReLU(name=f'conv_add_upsample_block_{block_id}_relu_{counter}_output')]

        # second is the result of upsampling

    def call(self, inputs, training=None, mask=None, *args, **kwargs):
        x_encode = inputs[0]
        x_upsample = inputs[1]

        x_encode = self.encoder_conv(x_encode)
        x_encode = self.encoder_bn(x_encode)
        x_encode = self.encoder_relu(x_encode)

        # print(f'encoded: {x_encode.shape} | upsampled: {x_upsample.shape}' )

        if x_upsample is not None:
            x = self.add_encoded_to_up([x_encode, x_upsample])
        else:
            x = x_encode

        # upsample x
        x_upsample = self.up_sum(x)
        x_conv = x

        for layer in self.conv_output_path:
            x_conv = layer(x_conv)

        return x_conv, x_upsample


class FullDecoder(tf.keras.models.Model):
    def __init__(
            self, up_count: int = 256, up_size_increment: int = 2, conv_output_size=3,
            conv_output_count: int = 128, conv_output_convs_count: int = 2,
            conv_and_upsample_blocks_count: int = 4, conv_on_concat_count: int = 512,
            conv_on_concat_size: int = 3, spatial_dropout_rate: float = 0.3,
            init_stride: int = 2, init_max_pool_stride: int = 2, first_resnet_conv_stride: int = 2,
            num_classes: int = 10, **kwargs):

        super(FullDecoder, self).__init__(**kwargs)
        # self, up_count: int = 256, up_size: int = 2, conv_output_size=3,
        # conv_output_count: int = 128, conv_output_convs_count: int = 2, block_id: int = 0
        #

        # tODO maybe later add parametrization of up_size_increment
        assert up_size_increment == 2
        self.up_size_increment = up_size_increment

        self.hidden_layers = [
            ConvAddUpsampleBlock(
                up_count=up_count, up_size=self.up_size_increment, conv_output_size=conv_output_size,
                conv_output_count=conv_output_count, conv_output_convs_count=conv_output_convs_count, block_id=block_id)
            for block_id in range(conv_and_upsample_blocks_count)]

        # tODO parametrize depth
        upsampling_sizes = np.power(2, np.arange(0, 4)[::-1])

        self.upsampling_hidden_layers = [
            tf.keras.layers.UpSampling2D((up_size, up_size), name=f'full_decoder_upsample{up_size}x{up_size}_{counter}')
            for counter, up_size in enumerate(upsampling_sizes)]

        # self.concat_upsampled = tf.keras.layers.Concatenate(axis=-1, name='full_decoder_concat_upsampled')

        assert conv_on_concat_count > 1
        assert conv_on_concat_size > 1
        self.conv_on_concat_count = conv_on_concat_count
        self.conv_on_concat_size = conv_on_concat_size

        self.conv_on_concat = tf.keras.layers.Conv2D(
            self.conv_on_concat_count, (self.conv_on_concat_size, self.conv_on_concat_size), padding='same',
            kernel_initializer=tf.keras.initializers.glorot_uniform(seed=0),
            name=f'full_decoder_conv{self.conv_on_concat_size}x{self.conv_on_concat_size}_conv_on_concat')
        self.conv_on_concat_bn = tf.keras.layers.BatchNormalization(name=f'full_decoder_bn_conv_on_concat')
        self.conv_on_concat_relu = tf.keras.layers.ReLU(name=f'full_decoder_relu_conv_on_concat')

        # reduce dims to num classes
        assert num_classes > 1
        self.num_classes = num_classes
        self.conv_to_num_classes = \
            tf.keras.layers.Conv2D(
                num_classes, (1, 1), padding='same',  kernel_initializer=tf.keras.initializers.glorot_uniform(seed=0),
                name=f'full_decoder_conv_to_num_classes')
        self.conv_to_num_classes_bn = tf.keras.layers.BatchNormalization(name=f'full_decoder_conv_to_num_classes_bn')
        self.conv_to_num_classes_relu = tf.keras.layers.ReLU(name=f'full_decoder_conv_to_num_classes_relu')

        # spatial dropout
        assert spatial_dropout_rate > 0 and spatial_dropout_rate < 1
        self.spatial_dropout_rate = spatial_dropout_rate
        self.conv_to_num_classes_spatial_dropout = \
            tf.keras.layers.SpatialDropout2D(
                self.spatial_dropout_rate, name=f'full_decoder_conv_to_num_classes_spatial_dropout')

        # upsample to original size
        assert init_stride > 0
        assert init_max_pool_stride > 0
        assert first_resnet_conv_stride > 0
        self.init_stride = init_stride
        self.init_max_pool_stride = init_max_pool_stride
        self.first_resnet_conv_stride = first_resnet_conv_stride

        final_upsampling_tuple = tuple(
            self._get_upsample_to_orig_size(self.init_stride, self.init_max_pool_stride, self.first_resnet_conv_stride)
            for _ in range(2))

        self.upsample_to_orig_size = \
            tf.keras.layers.UpSampling2D(
                final_upsampling_tuple, interpolation='bilinear', name=f'full_decoder_upsample_to_orig_size')

        # 1x1 with softmax activation
        # activate with softmax
        self.softmax_for_classif = tf.keras.layers.Conv2D(
            self.num_classes, (1, 1), activation='softmax',
            kernel_initializer=tf.keras.initializers.glorot_uniform(seed=0),
            name='full_decoder_softmax')

    def _get_upsample_to_orig_size(self, init_stride, init_max_pool_stride, first_resnet_conv_stride):
        return int(init_stride * init_max_pool_stride * first_resnet_conv_stride)

    def call(self, inputs, training=None, mask=None, *args, **kwargs):
        # unpack encoder inputs from smallest to largest x, y size
        x_conv_outputs = []
        x_upsampled = None
        for conv_inputs, hidden_layer, upsample_hidden_layer in zip(inputs, self.hidden_layers, self.upsampling_hidden_layers):

            # prepare input for adding and upsampling block
            x = [conv_inputs, x_upsampled]
            # add, conv and upsample
            x_conv, x_upsampled = hidden_layer(x)
            # upsample conv output
            x_conv = upsample_hidden_layer(x_conv)
            # cache conv resutls for concatenation
            x_conv_outputs.append(x_conv)

        # concatenate x_conv_outputs
        # x = self.concat_upsampled([x_conv_outputs])
        x = tf.concat(x_conv_outputs, axis=-1)
        # conv on concatenated layers
        x = self.conv_on_concat(x)
        x = self.conv_on_concat_bn(x)
        x = self.conv_on_concat_relu(x)
        # conv 1x1 to num classes
        x = self.conv_to_num_classes(x)
        x = self.conv_to_num_classes_bn(x)
        x = self.conv_to_num_classes_relu(x)
        # spatial dropout
        x = self.conv_to_num_classes_spatial_dropout(x)
        # upsample to original size
        x = self.upsample_to_orig_size(x)
        # 1x1 conv with softmax
        x = self.softmax_for_classif(x)
        return x


if __name__ == '__main__':

    model = tf.keras.models.Sequential(
        [tf.keras.layers.InputLayer(input_shape=(64, 64, 3)),
         tf.keras.layers.UpSampling2D((4, 4)),
         tf.keras.layers.Conv2D(3, (1, 1), strides=4, padding='valid')])

    model.compile(optimizer='adam', loss='mse', metrics=['mean_squared_error'])
    res = model.predict(tf.random.normal((1, 64, 64, 3)))
    print(res.shape)

    # check that full decoder works

    inputs = [
        tf.random.normal((1, 8, 8, 512)),
        tf.random.normal((1, 16, 16, 256)),
        tf.random.normal((1, 32, 32, 128)),
        tf.random.normal((1, 64, 64, 64))
    ]

    input_shapes = [
        tf.keras.layers.Input(shape=(None, 8, 8, 512)),
        tf.keras.layers.Input(shape=(None, 16, 16, 256)),
        tf.keras.layers.Input(shape=(None, 32, 32, 128)),
        tf.keras.layers.Input(shape=(None, 64, 64, 64))]

    input_shapes = [
        (None, 8, 8, 512),
        (None, 16, 16, 256),
        (None, 32, 32, 128),
        (None, 64, 64, 64)]

    # input_shapes = [el.shape for el in inputs]

    fd = FullDecoder()
    fd.build(input_shape=input_shapes)
    optimizer = tf.keras.optimizers.Adam(learning_rate=0.001)
    fd.compile(optimizer=optimizer, loss='sparse_categorical_entropy', metrics=['accuracy'])
    res = fd.call(inputs)
    print(res.shape)


    # upsampling_block = ConvAddUpsampleBlock()
    # decoder_output = tf.random.normal((1, 32, 32, 256))
    # upconv_output = tf.random.normal((1, 32, 32, 256))
    #
    # upsampling_conv_path, upsampling__up_path = upsampling_block([decoder_output, upconv_output])
    # print('### upsampling_conv_path.shape ###')
    # print(upsampling_conv_path.shape)
    # print('### upsampling__up_path.shape ###')
    # print(upsampling__up_path.shape)

    pass
