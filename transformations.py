from copy import deepcopy
import cv2
import dask.bag as db
from distributed import Client
import json
import numpy as np
import os
import pandas as pd
import tensorflow as tf
import tensorflow_addons as tfa
from tqdm import tqdm

from paths import BASE_PATH
from visualization import plot_observation, save_observation_plot


TRAIN_SAMPLES_MOVED = 20
SOURCE_DIR = BASE_PATH
# SOURCE_DIR = '/home/kw/Projects/drone_cv/agriculture-vision/supervised/labeled_data/'

SETS_TO_DIRS = {
    'train': 'train',
    'test': 'test',
    'validation': 'val'}

ALLOWED_ROTATION_ANGLES = [45, 90, 135, -45, -90, -135]
ALLOWED_TRANSFORMATIONS = ['rotate', 'flip_h', 'flip_v']

# TRAIN_DIR = 'train'
# TEST_DIR = 'test'
# VAL_DIR = 'val'

SOURCE_RGB_INPUT_DIR = 'images/rgb'
SOURCE_NIR_INPUT_DIR = 'images/nir'

SOURCE_LABELS_DIR = 'labels'
SOURCE_MASKS_DIR = 'masks'
SOURCE_BOUNDARIES_DIR = 'boundaries'

TARGET_TRAIN_DIR = 'data/train'

TARGET_INPUT_DIR = 'input'
TARGET_LABELS_DIR = 'labels'

DESIRED_EXTENSION = 'png'

ALLOWED_SET_TYPES = ['train', 'test', 'validation']


def resize_and_normalize_image(image, size: int = 512):
    image = tf.cast(image, tf.float32)
    image = image/255.0
    # resize image
    image = tf.image.resize(image, (size, size))
    return image


def resize_mask(mask, size: int = 512):
    mask = tf.expand_dims(mask, axis=-1)
    mask = tf.image.resize(mask, (512, 512))
    mask = tf.cast(mask, tf.uint8)
    return mask


def denormalize_image(image):
    # assuming shape (X, X, N)
    assert image.shape[-1] > 0 and image.shape[-1] <= 3

    denorm_image = np.zeros(image.shape)

    for channel in range(image.shape[-1]):
        image_channel = image[:, :, channel]
        if (np.max(image_channel) - np.min(image_channel)) != 0:
            denorm_image[:, :, channel] = \
                255 * (image_channel - np.min(image_channel)) / (np.max(image_channel) - np.min(image_channel))
        else:
            denorm_image[:, :, channel] = 0

    return denorm_image


def get_single_label_channel(label):
    # has shape of X, X, 3
    assert len(label.shape) == 3

    if label.shape[-1] == 1:
        return label

    np.testing.assert_array_equal(label[:, :, 0], label[:, :, 1])
    np.testing.assert_array_equal(label[:, :, 0], label[:, :, 2])

    return label[:, :, 0]


def assign_class_labels(not_scaled_one_hot_labels, max_class_channel_value):
    assert len(not_scaled_one_hot_labels.shape) == 3
    one_hot_labels = not_scaled_one_hot_labels / max_class_channel_value
    num_channels = not_scaled_one_hot_labels.shape[-1]
    for class_idx in range(num_channels):
        one_hot_labels[:, :, class_idx] *= (class_idx + 1)
    return one_hot_labels


def repair_overlapping_labels(single_channel_labels, max_class_channel_value: int = 255, flatten_result=False):
    # TODO class labels should start at 1, background is class 0
    # stack single channel layers
    single_channel_class_labels = \
        assign_class_labels(
            not_scaled_one_hot_labels=single_channel_labels,
            max_class_channel_value=max_class_channel_value)

    # sum labels to get overlaps -> 0 - 1 map of overlaps
    if np.sum(np.sum(single_channel_labels, axis=-1) > max_class_channel_value) == 0:
        if not flatten_result:
            return single_channel_class_labels, False
        return np.sum(single_channel_class_labels, axis=-1), False

    area_per_class = {
        class_idx: area
        for class_idx, area
        in enumerate(np.sum(single_channel_labels / max_class_channel_value, axis=(0, 1)))}
    # this will create a map of x, y, z coords for each non-zero value
    non_zero_indices = np.argwhere(single_channel_class_labels)
    # sort by class column
    sorted_non_zero_indices_by_class = non_zero_indices[np.argsort(non_zero_indices[:, -1])]
    # get both class labels and splits for those labels
    class_indices, splits = np.unique(sorted_non_zero_indices_by_class[:, -1], return_index=True)

    per_class_overlapping_xy = {
        class_label: overlap_xy_coords for class_label, overlap_xy_coords in
        zip(class_indices, np.split(sorted_non_zero_indices_by_class[:, :-1], splits[1:]))}

    for reference_class_index in class_indices:
        for overlapping_class_index in class_indices:
            # if the comparison has already been made continue
            if overlapping_class_index <= reference_class_index:
                continue

            reference_label_xy_set = \
                set([tuple(xy) for xy in per_class_overlapping_xy[reference_class_index].tolist()])
            overlapping_label_xy_set = \
                set([tuple(xy) for xy in per_class_overlapping_xy[overlapping_class_index].tolist()])

            reference_and_overlap_xy_intersection = reference_label_xy_set.intersection(overlapping_label_xy_set)

            if not reference_and_overlap_xy_intersection:
                continue

            reference_class_area = area_per_class[reference_class_index]
            overlapping_class_area = area_per_class[overlapping_class_index]

            # determine which class will have intersection subtracted
            # this version writes zeros to smaller class
            # trimmed_class_index = \
            #     overlapping_class_index if reference_class_area >= overlapping_class_area \
            #     else reference_class_index

            # determine which class will have intersection subtracted
            # this version writes zeros to larger class
            zeroed_intersection_class_index = \
                reference_class_index if reference_class_area >= overlapping_class_area \
                else overlapping_class_index
            # set zeros to all x, y coordinates smaller class
            x, y = np.array(list(reference_and_overlap_xy_intersection)).T

            single_channel_class_labels[x, y, zeroed_intersection_class_index] = 0

    if not flatten_result:
        return single_channel_class_labels, True
    return np.sum(single_channel_class_labels, axis=-1), True


def get_observation_images(
        input_image_name, classes, max_class_channel_value: int = 255, set_type: str = 'train',
        scale_label: bool = False):
    assert set_type in SETS_TO_DIRS

    # those are jpg files
    rgb_image = cv2.imread(os.sep.join([SOURCE_DIR, SETS_TO_DIRS[set_type], SOURCE_RGB_INPUT_DIR, input_image_name]))

    nir_image = cv2.imread(os.sep.join([SOURCE_DIR, SETS_TO_DIRS[set_type], SOURCE_NIR_INPUT_DIR, input_image_name]))
    # those are png files
    labels_images = [
        cv2.imread(os.sep.join(
            [SOURCE_DIR, SETS_TO_DIRS[set_type], SOURCE_LABELS_DIR, c, input_image_name.replace('.jpg', '.png')]))
        for c in classes]

    single_channel_labels = np.stack([get_single_label_channel(scl) for scl in labels_images], axis=-1)

    flat_label, has_overlaps = \
        repair_overlapping_labels(
            single_channel_labels=single_channel_labels,
            max_class_channel_value=max_class_channel_value, flatten_result=True)

    if scale_label:
        num_classes = len(labels_images)
        flat_label *= int(max_class_channel_value / num_classes)
    mask_image = \
        cv2.imread(os.sep.join([SOURCE_DIR, SETS_TO_DIRS[set_type], SOURCE_MASKS_DIR, input_image_name.replace('.jpg', '.png')]))
    boundary_image = \
        cv2.imread(os.sep.join([SOURCE_DIR, SETS_TO_DIRS[set_type], SOURCE_BOUNDARIES_DIR, input_image_name.replace('.jpg', '.png')]))

    return rgb_image, nir_image, flat_label, mask_image, boundary_image, has_overlaps


def get_gen_size(set_type: str = 'train'):
    rgb_images_source_dir = os.sep.join([SOURCE_DIR, SETS_TO_DIRS[set_type], SOURCE_RGB_INPUT_DIR])
    return len(os.listdir(rgb_images_source_dir))


# autographed function to get preprocessed images
def get_rgb_nir_label(
        input_image_name, classes, max_class_channel_value: int = 255, set_type: str = 'train'):
    rgb_image, nir_image, flat_label, _, _, _ = \
        get_observation_images(
            input_image_name=input_image_name, classes=classes,
            max_class_channel_value=max_class_channel_value, set_type=set_type)
    normalized_rgb_image, normalized_nir_image = \
        [resize_and_normalize_image(image) for image in [rgb_image, nir_image]]
    return normalized_rgb_image, normalized_nir_image, flat_label.astype(np.int32)


def _transform_image_and_label(image, label):
    all_possible_transformations_count = \
        len(ALLOWED_ROTATION_ANGLES) + len(ALLOWED_TRANSFORMATIONS) - 1
    probas = [len(ALLOWED_ROTATION_ANGLES) / all_possible_transformations_count, 1 / all_possible_transformations_count, 1 / all_possible_transformations_count]
    transformation = np.random.choice(ALLOWED_TRANSFORMATIONS, p=probas)
    if transformation == 'rotate':
        angle = np.random.choice(ALLOWED_ROTATION_ANGLES)
        if angle in [90, -90]:
            k = angle / 90
            return tf.image.rot90(image, k=k), tf.image.rot90(label, k=k)
        else:
            return tfa.image.rotate(image, -1 * angle / 360 * 2 * np.pi), \
                   tfa.image.rotate(label, -1 * angle / 360 * 2 * np.pi)
    elif transformation == 'flip_h':
        return tf.image.flip_left_right(image), tf.image.flip_left_right(label)
    elif transformation == 'flip_v':
        return tf.image.flip_up_down(image), tf.image.flip_up_down(label)


# TODO path hardcode should be removed
def generate_rgb_nir_concat_and_label_for_set(
        set_type: str = 'train', concat_rgb_nir: bool = True,
        images_per_epoch: int = 100000,
        images_info_json_filename_pattern: str = 'utils/{}_class_filenames.json'):
    """
    Make generator so tf can use it to make data generator as a model input

    Parameters
    ----------
    set_type
    concat_rgb_and_nir

    Returns
    -------

    """
    if isinstance(set_type, bytes):
        set_type = set_type.decode('utf-8')
    assert set_type in SETS_TO_DIRS
    # get images list for desired set
    rgb_images_source_dir = os.sep.join([SOURCE_DIR, SETS_TO_DIRS[set_type], SOURCE_RGB_INPUT_DIR])
    rgb_images_list = os.listdir(rgb_images_source_dir)

    # get labels for selected images
    labels_super_dir = os.sep.join([SOURCE_DIR, SETS_TO_DIRS[set_type], SOURCE_LABELS_DIR])
    classes = os.listdir(labels_super_dir)
    # for each input image

    if set_type == 'validation':
        np.random.shuffle(rgb_images_list)
        rgb_images_list = rgb_images_list[:1000]

    print('### READING DATA INFO ###')
    with open(images_info_json_filename_pattern.format(set_type), 'r') as iif:
        labels_info = \
            {label: [image_filename for image_filename in images_for_label
                     if image_filename in rgb_images_list]
             for label, images_for_label in json.loads(iif.read()).items()}

    # has_seen_orig = {label: [] for label in labels_info.keys()}
    labels = list(labels_info.keys())

    label_names = np.array(labels)[np.random.randint(0, len(labels), images_per_epoch)]
    input_image_names = [np.random.choice(labels_info[label_name]) for label_name in label_names]

    # for _ in range(images_per_epoch):
    for input_image_name in input_image_names:

        # select random label images
        # label_name = np.random.choice(labels)
        # input_image_name = np.random.choice(labels_info[label_name])

        rgb, nir, label = \
            get_rgb_nir_label(input_image_name, classes, set_type=set_type)

        if concat_rgb_nir:
            yielded_image = tf.concat([rgb, nir], axis=-1)
        else:
            yielded_image = rgb
        # add last dim to flat labels
        yielded_label = tf.cast(np.expand_dims(label, axis=-1), tf.int32)
        # assert 1 >= tf.math.reduce_min(yielded_rgb_nir) >= 0
        # assert 1 >= tf.math.reduce_max(yielded_rgb_nir) >= 0

        # transform 25% of dataset
        if np.random.rand() <= 0.25 and set_type == 'train':
            yielded_image, yielded_label = \
                _transform_image_and_label(yielded_image, yielded_label)

        # TODO this processes original data first and then transforms it
        # if input_image_name in has_seen_orig[label_name]:
        #     # if this image has already been seen rotate / transform
        #     yielded_image, yielded_label = \
        #         _transform_image_and_label(yielded_image, yielded_label)
        # else:
        #     has_seen_orig[label_name].append(input_image_name)

        yield yielded_image, yielded_label


if __name__ == '__main__':
    # list train rgb images
    source_input_images_path = os.sep.join([SOURCE_DIR, SETS_TO_DIRS['train'], SOURCE_RGB_INPUT_DIR])
    input_images = os.listdir(source_input_images_path)

    # get labels for selected images
    labels_super_dir = os.sep.join([SOURCE_DIR, SETS_TO_DIRS['train'], SOURCE_LABELS_DIR])
    classes = os.listdir(labels_super_dir)

    # # load overlapping labels csv
    # overlapping_labels_df = pd.read_csv('overlapping_labels.csv')
    # # example overlapping plot
    # overlapping_image_names = overlapping_labels_df['image_name']

    # # TODO this chunk saves overlapping images
    # # clean save dir
    # save_dir = 'data/overlapping_labels'
    # os.system(f'rm -rf {save_dir}/*')
    # for oin in tqdm(overlapping_image_names):
    #     obs_tuple = get_observation_images(oin, classes=classes, scale_label=True)
    #     save_observation_plot(image_name=oin, observation_tuple=obs_tuple[:-1])
    #     # plot_observation(obs_tuple[:-1])

    # TODO process and save images

    # TODO this chunk gets overlaps
    # c = Client()
    # print(c.dashboard_link)
    # res = db.from_sequence(input_images).map(get_all_images_for_observation, **{'classes': classes}).compute()
    # overlapping_labels = [r for r in res if r[1]]
    # if overlapping_labels:
    #     pd.DataFrame(
    #         {'image_name': [ol[0] for ol in overlapping_labels],
    #          'has_overlaps': [ol[1] for ol in overlapping_labels]})\
    #         .to_csv('overlapping_labels.csv', index=False)
    # # print(sum(res))

    # TODO this chunk checks data generator
    # TODO implement tests
    counter = 0
    for rgb, label in tqdm(generate_rgb_nir_concat_and_label_for_set(
            set_type='validation', concat_rgb_nir=False), total=100000):

        # print(label)
        # print(label.dtype)
        # break

        filler = np.zeros(label.shape[:-1])
        label_3d = np.stack([label.numpy()[:, :, 0], filler, filler], axis=-1)
        print(label_3d.shape)
        print('### rgb.numpy().max() ###')
        print(rgb.numpy().max())
        res = np.hstack([rgb.numpy(), label_3d])
        print(res.max())
        print(res.shape)

        cv2.imshow('window', res)
        cv2.waitKey(0)
        # input('sanity check')

        # break

        # if np.isnan(rgb).any():
        #     print('NANS IN RGB')
        #
        # if np.isnan(label).any():
        #     print('NANS IN LABEL')
        # print(f'### RGB shape: {rgb.shape} | NIR shape: {nir.shape} | label shape: {label.shape} ###')
        # counter += 1
        # if counter > 50:
        #     break
