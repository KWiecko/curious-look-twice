import os

BASE_PATH = '/home/kw/Projects/drone_cv/agriculture-vision/supervised/labeled_data'

CHECK_PATH = 'train/boundaries/'
TRAIN_BOUNDARIES_PATH = 'train/boundaries/'
TRAIN_IMAGES_PATH = 'train/images/rgb/'
TRAIN_LABELS_PATH = 'train/labels/'
TRAIN_MASKS_PATH = 'train/masks/'
VAL_BOUNDARIES_PATH = 'val/boundaries/'
VAL_IMAGES_PATH = 'val/images/rgb/'
VAL_LABELS_PATH = 'val/labels/'
VAL_MASKS_PATH = 'val/masks/'
TEST_BOUNDARIES_PATH = 'test/boundaries/'
TEST_IMAGES_PATH = 'test/images/rgb/'
TEST_LABELS_PATH = 'test/labels/'
TEST_MASKS_PATH = 'test/masks/'

IMG_TRAIN_PATH = BASE_PATH + '/' + 'UNET/img/train/'
IMG_VAL_PATH = BASE_PATH + '/' + 'UNET/img/val/'
IMG_TEST_PATH = BASE_PATH + '/' + 'UNET/img/test/'
MASK_TRAIN_PATH = BASE_PATH + '/' + 'UNET/mask/train/'
MASK_VAL_PATH = BASE_PATH + '/' + 'UNET/mask/val/'
MASK_TEST_PATH = BASE_PATH + '/' + 'UNET/mask/test/'

OBLIGATORY_PATHS = [
    IMG_TRAIN_PATH,
    IMG_VAL_PATH,
    IMG_TEST_PATH,
    MASK_TRAIN_PATH,
    MASK_VAL_PATH,
    MASK_TEST_PATH]


def make_dirs():
    for dir_name in OBLIGATORY_PATHS:
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)
