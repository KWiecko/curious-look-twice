from setuptools import find_packages, setup


if __name__ == '__main__':
    setup(name='agro-cv',
          version='0.1',
          description='bam',
          author='PB+KW',
          packages=
          find_packages(exclude=['*tests*', '*local_test*', '*tools*', '*example_data*', '*tools*']))
